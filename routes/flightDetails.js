/**
 * Created by ardacuhadaroglu on 18/05/16.
 */

var express = require('express');
var router = express.Router();

/* GET users listing. */
router.post('/', function(req, res, next) {

    console.log(req.body.flight_number);
    
    var flight_details = {};
    flight_details.flight_no = req.body.flight_number;
    flight_details.airline = "Turkish Airline";
    flight_details.from = "Ankara";
    flight_details.to = "İstanbul";
    flight_details.date = "28 May 2016 - 11:00";
    flight_details.aircraft_model = "Boeing 737";
    flight_details.registration = "RA-65565";
    flight_details.status = "Planned";


    res.send(JSON.stringify(flight_details));
});

module.exports = router;
